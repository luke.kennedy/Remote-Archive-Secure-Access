# Remote Archive Secure Access

Automatic synchronisation of local unencrypted files to and from long-term low-access encrypted storage, such as S3 Glacier.

If you have a collection of data which:

- needs to be accessed regularly on your local file system
- changes slowly over time
- needs to be backed up
- cannot be stored unencrypted
- occasionally needs to be replicatd to a different machine or a replicate needs to be updated (but not often)

RASA might help you.

## Overview

RASA creates a manifest of file names, sizes, timestamps and checksums. This manifest, once uploaded in encrypted form, is used to identify encrypted files to upload or download from the secure storage option if needed, when compared to the local manifest and local file system.