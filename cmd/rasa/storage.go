package main

type Storage interface {
	List() (identifiers []string, err error)
	CheckIfExists(identifier string) (exists bool, err error)
	Upload(data []byte) (identifier string, err error)
	Download(identifier string) (data []byte, err error)
	Replace(identifier string, data []byte) (err error)
}
