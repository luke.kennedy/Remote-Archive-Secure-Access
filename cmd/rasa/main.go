package main

import (
	"encoding/json"
	"log"
	"os"

	"github.com/spf13/cobra"
)

var DefaultTempStorage = os.TempDir() + "/rasa"
var basePath = "."

func main() {
	rootCmd := &cobra.Command{}
	storageLocation := rootCmd.Flags().StringP("storage", "s", DefaultTempStorage, "")
	rootCmd.RunE = func(cmd *cobra.Command, args []string) error {
		s := LocalStorage{
			BasePath: *storageLocation,
		}
		exists, err := s.StorageExists()
		if err != nil {
			return err
		}
		if exists {
			err := s.PurgeStorage()
			if err != nil {
				return err
			}
		}
		err = s.CreateStorage()
		if err != nil {
			return err
		}
		_, err = s.Upload([]byte("abc"))
		if err != nil {
			return err
		}
		_, err = s.List()
		if err != nil {
			return err
		}
		m, err := CreateManifest(basePath)
		if err != nil {
			return err
		}
		jsonM, err := json.Marshal(m)
		if err != nil {
			return err
		}
		// stuff
		log.Println(string(jsonM))
		return NewBackup(basePath, m, s)
	}
	rootCmd.Execute()
}
