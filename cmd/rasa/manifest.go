package main

import (
	"crypto/sha256"
	"fmt"
	"io"
	"io/fs"
	"log"
	"os"
	"time"
)

type Manifest struct {
	Version   int // Unused, may be used in future to handle existing manifests across updates of this tool
	Name      string
	BasePath  string
	CreatedAt time.Time
	UpdatedAt time.Time
	Files     []FileRecord
}

type ChecksumTypeIdentifier string

const (
	ChecksumTypeInvalid ChecksumTypeIdentifier = ""
	ChecksumTypeSHA256  ChecksumTypeIdentifier = "SHA256"
)

type FileRecord struct {
	Name              string
	StorageIdentifier string
	RelativePath      string
	SizeBytes         int64
	Checksum          []byte
	ChecksumMethod    ChecksumTypeIdentifier
	UpdatedAt         time.Time
}

func CreateManifest(basePath string) (m Manifest, err error) {
	osfs := os.DirFS(basePath)
	entries, readErr := readDir(osfs, ".")
	if readErr != nil {
		return m, readErr
	}
	m.BasePath = basePath
	log.Printf("Found %d files, statting and hashing them all, this may take a while...\n", len(entries))
	breakPointSize := len(entries) / 100
	for i := range entries {
		if breakPointSize != 0 && i%breakPointSize == 0 {
			log.Printf("%d%%\n", i/breakPointSize)
		}
		entry := &entries[i]
		stats, err := fs.Stat(osfs, entry.RelativePath)
		if err != nil {
			return m, fmt.Errorf("failed to get file info for %s: %w", entry.RelativePath, err)
		}
		entry.SizeBytes = stats.Size()
		entry.UpdatedAt = stats.ModTime()
		entry.Checksum, err = hashFile(osfs, entry.RelativePath)
		if err != nil {
			return m, fmt.Errorf("failed to hash file %s: %w", entry.RelativePath, err)
		}
		entry.ChecksumMethod = ChecksumTypeSHA256
	}
	m.Files = entries
	return m, nil
}

func readDir(fsys fs.FS, basePath string) ([]FileRecord, error) {
	records := []FileRecord{}
	entries, readErr := fs.ReadDir(fsys, basePath)
	if readErr != nil {
		return nil, readErr
	}
	for _, entry := range entries {
		if entry.Name() == ".git" {
			continue
		}
		if entry.IsDir() {
			newPath := entry.Name()
			if basePath != "." {
				newPath = basePath + "/" + newPath
			}
			extraEntries, readErr := readDir(fsys, newPath)
			if readErr != nil {
				panic(readErr)
			}
			records = append(records, extraEntries...)
		} else {
			prefix := basePath + "/"
			if basePath == "." {
				prefix = ""
			}
			newRecord := FileRecord{
				Name:         entry.Name(),
				RelativePath: prefix + entry.Name(),
			}
			records = append(records, newRecord)
		}
	}
	return records, nil
}

const NumBlocksToRead = 1024 * 1024

func hashFile(fsys fs.FS, path string) (hash []byte, err error) {
	f, err := fsys.Open(path)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	hasher := sha256.New()
	data := make([]byte, NumBlocksToRead*hasher.BlockSize())
	for {
		clear(data)
		n, readErr := f.Read(data)
		if readErr != nil && readErr != io.EOF {
			return nil, readErr
		}
		_, err = hasher.Write(data[:n])
		if err != nil {
			return nil, err
		}
		if readErr == io.EOF {
			return hasher.Sum([]byte{}), nil
		}
	}
}
