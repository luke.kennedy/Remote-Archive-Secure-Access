package main

import (
	"encoding/json"
	"fmt"
	"os"
)

func NewBackup(basePath string, m Manifest, s Storage) error {
	data, err := json.MarshalIndent(m, "", "  ")
	if err != nil {
		return err
	}
	err = os.WriteFile(basePath+"/"+"manifest.json", data, os.ModeExclusive)
	if err != nil {
		return err
	}
	return fmt.Errorf("not implemented")
}
