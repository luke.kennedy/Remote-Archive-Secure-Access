package main

import (
	"errors"
	"fmt"
	"io/fs"
	"os"

	uuid "github.com/satori/go.uuid"
)

type LocalStorage struct {
	BasePath string
}

func (ls LocalStorage) StorageExists() (exists bool, err error) {
	info, infoErr := os.Stat(ls.BasePath)
	if infoErr != nil && errors.Is(infoErr, os.ErrExist) {
		return false, nil
	}
	if infoErr != nil {
		return false, infoErr
	}
	if !info.IsDir() {
		return false, fmt.Errorf("not a directory")
	}
	return true, nil
}

func (ls LocalStorage) CreateStorage() (err error) {
	return os.Mkdir(ls.BasePath, os.ModeExclusive)
}

func (ls LocalStorage) PurgeStorage() (err error) {
	if ls.BasePath == "" {
		return fmt.Errorf("cannot delete empty storage")
	}
	return os.RemoveAll(ls.BasePath)
}

func (ls LocalStorage) List() (identifiers []string, err error) {
	entries, readErr := fs.ReadDir(os.DirFS(ls.BasePath), ".")
	if readErr != nil {
		err = readErr
		return
	}
	for _, entry := range entries {
		if !entry.IsDir() {
			identifiers = append(identifiers, entry.Name())
		}
	}
	return
}

func (ls LocalStorage) CheckIfExists(identifier string) (exists bool, err error) {
	_, err = os.Stat(ls.BasePath + "/" + identifier)
	if err == nil {
		exists = true
	} else if errors.Is(err, os.ErrExist) {
		err = nil
	}
	return
}

func (ls LocalStorage) Upload(data []byte) (identifier string, err error) {
	identifier = uuid.NewV4().String()
	return identifier, ls.createFile(identifier, data)
}

func (ls LocalStorage) createFile(identifier string, data []byte) error {
	f, err := os.Create(ls.BasePath + "/" + identifier)
	if err != nil {
		return err
	}
	defer f.Close()
	_, err = f.Write(data)
	if err != nil {
		f.Close()
		os.Remove(identifier)
		return err
	}
	return nil
}

func (ls LocalStorage) Download(identifier string) (data []byte, err error) {
	return fs.ReadFile(os.DirFS(ls.BasePath), identifier)
}

func (ls LocalStorage) Replace(identifier string, data []byte) (err error) {
	err = os.Remove(ls.BasePath + "/" + identifier)
	if err != nil {
		return err
	}
	return ls.createFile(identifier, data)
}
